package com.impaqgroup.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;

import java.util.Collection;

/**
 * Created by lguz on 04.04.16.
 */
@Configuration
public class CarFlowConfiguration {

    @MessagingGateway
    public interface Upcase {

        @Gateway(requestChannel = "upcase.input")
        Collection<String> upcase(Collection<String> strings);
    }

    @Bean
    public IntegrationFlow upcase() {
        return f ->
                f.split()
                .<String, String>transform(String::toUpperCase)
                .aggregate();
    }
}

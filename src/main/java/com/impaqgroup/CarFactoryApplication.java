package com.impaqgroup;

import com.impaqgroup.service.CarDispatcher;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;

import java.util.Arrays;

@SpringBootApplication
@EnableIntegration
@IntegrationComponentScan
public class CarFactoryApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = new SpringApplicationBuilder(CarFactoryApplication.class)
				.web(false)
				.run(args);

		CarDispatcher carDispatcher = ctx.getBean(CarDispatcher.class);
		carDispatcher.run(Arrays.asList("siema", "ziom"));
	}
}

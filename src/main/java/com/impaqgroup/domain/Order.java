package com.impaqgroup.domain;

import java.util.Optional;

public class Order {

    private Long id;
    private String model;
    private int doors;
    private Optional<Car> car = Optional.empty();

    public Order(Long id, String model, int doors) {
        this.id = id;
        this.model = model;
        this.doors = doors;
    }

    public Long getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public int getDoors() {
        return doors;
    }

    public Optional<Car> getCar() {
        return car;
    }
}

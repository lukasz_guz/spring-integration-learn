package com.impaqgroup.service;

import com.impaqgroup.configuration.CarFlowConfiguration.Upcase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * Created by lguz on 04.04.16.
 */
@Service
public class CarDispatcher {

    private Upcase gateway;

    @Autowired
    public CarDispatcher(Upcase gateway) {
        this.gateway = gateway;
    }

    public void run(List<String> args) {
        Collection<String> upcaseResult = gateway.upcase(args);
        System.out.println(upcaseResult);
    }
}
